package adf.sample.view;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.event.ClientListenerSet;
import oracle.adf.view.rich.render.ClientEvent;


public class CustomBean {
    private RichInputText secondSpyField;

    public CustomBean() {
    }
    
    public void handleRequest(ClientEvent event){
        
        System.out.println("---"+event.getParameters().get("payload"));
        
    }

    public void setSecondSpyField(RichInputText secondSpyField) {
        this.secondSpyField = secondSpyField;
        if (this.secondSpyField!=null){
            ClientListenerSet cls = new ClientListenerSet();
            cls.addListener("keyUp","clientMethodCall");
            cls.addCustomServerListener("customEvent",getMethodExpression("#{customBean.handleRequest}"));
            this.secondSpyField.setClientListeners(cls);
        }
    }

    public RichInputText getSecondSpyField() {
        return secondSpyField;
    }
    
    
    public MethodExpression getMethodExpression(String s){
        FacesContext fc = FacesContext.getCurrentInstance();       
        ELContext elctx  = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();       
        MethodExpression methodExpr = elFactory.createMethodExpression(elctx,s,null,new Class[]{ClientEvent.class});       
        return methodExpr;
    }
}
