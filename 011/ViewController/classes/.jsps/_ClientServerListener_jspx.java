
import oracle.jsp.runtime.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import oracle.jsp.el.*;
import javax.el.*;


public class _ClientServerListener_jspx extends com.orionserver.http.OrionHttpJspPage {


  // ** Begin Declarations


  // ** End Declarations

  public void _jspService(HttpServletRequest request, HttpServletResponse response) throws java.io.IOException, ServletException {

    response.setContentType( "text/html;charset=windows-1252");
    /* set up the intrinsic variables using the pageContext goober:
    ** session = HttpSession
    ** application = ServletContext
    ** out = JspWriter
    ** page = this
    ** config = ServletConfig
    ** all session/app beans declared in globals.jsa
    */
    PageContext pageContext = JspFactory.getDefaultFactory().getPageContext( this, request, response, null, true, JspWriter.DEFAULT_BUFFER, true);
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    int __jsp_tag_starteval;
    ServletContext application = pageContext.getServletContext();
    JspWriter out = pageContext.getOut();
    _ClientServerListener_jspx page = this;
    ServletConfig config = pageContext.getServletConfig();

    try {
      // compile time tag reuse - begin
      JspTag[] __ojsp_classicTags = new JspTag[11];
      // usage : com.sun.faces.taglib.jsf_core.ViewTag
      com.sun.faces.taglib.jsf_core.ViewTag _ctru0 = (com.sun.faces.taglib.jsf_core.ViewTag) OracleJspRuntime.getTagHandler(pageContext, com.sun.faces.taglib.jsf_core.ViewTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag
      oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag.class, "compiletime");
      // usage : javax.faces.webapp.FacetTag name
      javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) OracleJspRuntime.getTagHandler(pageContext, javax.faces.webapp.FacetTag.class, "compiletime");
      // usage : org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag
      org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag _ctru3 = (org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag) OracleJspRuntime.getTagHandler(pageContext, org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag
      oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag
      oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag label
      oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag type method
      oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag _ctru7 = (oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag type method
      oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag _ctru8 = (oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag
      oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag _ctru9 = (oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag binding label
      oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru10 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag.class, "compiletime");
      // compile tag reuse - end


      if (_ctru0_helper_1(pageContext, __ojsp_classicTags, _ctru0, _ctru1, _ctru2, _ctru3, _ctru4, _ctru5, _ctru2, _ctru6, _ctru7, _ctru8, _ctru9, _ctru10))
        return;

    }
    catch (java.lang.Throwable e) {
      if (!(e instanceof javax.servlet.jsp.SkipPageException)){
        try {
          if (out != null) out.clear();
        }
        catch (java.lang.Exception clearException) {
        }
        pageContext.handlePageException(e);
      }
    }
    finally {
      OracleJspRuntime.extraHandlePCFinally(pageContext, true);
      JspFactory.getDefaultFactory().releasePageContext(pageContext);
    }

  }
    
  private boolean _ctru0_helper_1(PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    com.sun.faces.taglib.jsf_core.ViewTag _ctru0 = (com.sun.faces.taglib.jsf_core.ViewTag) tags[0];
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[2];
    oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag _ctru7 = (oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag) tags[8];
    oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag _ctru8 = (oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag) tags[9];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag) tags[4];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru10 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[11];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[7];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[5];
    oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag _ctru9 = (oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag) tags[10];
    org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag _ctru3 = (org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag) tags[3];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru0.setPageContext(pageContext);
      _ctru0.setParent(null);
((JspIdConsumer) _ctru0).setJspId("_ctru0");
      __jsp_tag_starteval=_ctru0.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru0,__jsp_tag_starteval,out);
        do {
          if (_ctru1_helper_2(_ctru0, pageContext, __ojsp_classicTags, _ctru1, _ctru2, _ctru3, _ctru4, _ctru5, _ctru2, _ctru6, _ctru7, _ctru8, _ctru9, _ctru10))
            return true;
        } while (_ctru0.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru0.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru1_helper_2(com.sun.faces.taglib.jsf_core.ViewTag _ctru0, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[1];
    oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag _ctru7 = (oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag) tags[7];
    oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag _ctru8 = (oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag) tags[8];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag) tags[0];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag) tags[3];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru10 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[10];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[6];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[4];
    oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag _ctru9 = (oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag) tags[9];
    org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag _ctru3 = (org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag) tags[2];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru1.setPageContext(pageContext);
      _ctru1.setParent(_ctru0);
((JspIdConsumer) _ctru1).setJspId("_ctru1");
      __jsp_tag_starteval=_ctru1.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru1,__jsp_tag_starteval,out);
        do {
          if (_ctru2_helper_3(_ctru1, pageContext, __ojsp_classicTags, _ctru2, _ctru3))
            return true;
          if (_ctru4_helper_4(_ctru1, pageContext, __ojsp_classicTags, _ctru4, _ctru5, _ctru2, _ctru6, _ctru7, _ctru8, _ctru9, _ctru10))
            return true;
        } while (_ctru1.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru1.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru2_helper_3(oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[0];
    org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag _ctru3 = (org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag) tags[1];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru2.setPageContext(pageContext);
      _ctru2.setParent(_ctru1);
      _ctru2.setName("metaContainer");
      __jsp_tag_starteval=_ctru2.doStartTag();
      if (OracleJspRuntime.checkStartTagEval(__jsp_tag_starteval))
      {
        do {
          if (_ctru3_helper_5(_ctru2, pageContext, __ojsp_classicTags, _ctru3))
            return true;
        } while (_ctru2.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
      }
      if (_ctru2.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru3_helper_5(javax.faces.webapp.FacetTag _ctru2, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag _ctru3 = (org.apache.myfaces.trinidadinternal.taglib.UIXGroupTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru3.setPageContext(pageContext);
      _ctru3.setParent(_ctru2);
((JspIdConsumer) _ctru3).setJspId("_ctru3");
      __jsp_tag_starteval=_ctru3.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru3,__jsp_tag_starteval,out);
        do {
          out.write(__oracle_jsp_text[0]);
        } while (_ctru3.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru3.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru4_helper_4(oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[2];
    oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag _ctru7 = (oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag) tags[4];
    oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag _ctru8 = (oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag) tags[5];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag) tags[0];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru10 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[7];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[3];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag _ctru9 = (oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag) tags[6];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru4.setPageContext(pageContext);
      _ctru4.setParent(_ctru1);
((JspIdConsumer) _ctru4).setJspId("_ctru4");
      __jsp_tag_starteval=_ctru4.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru4,__jsp_tag_starteval,out);
        do {
          if (_ctru5_helper_6(_ctru4, pageContext, __ojsp_classicTags, _ctru5, _ctru2, _ctru6, _ctru7, _ctru8, _ctru9, _ctru10))
            return true;
        } while (_ctru4.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru4.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru5_helper_6(oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru4, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[1];
    oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag _ctru7 = (oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag) tags[3];
    oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag _ctru8 = (oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag) tags[4];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru10 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[6];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[2];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[0];
    oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag _ctru9 = (oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag) tags[5];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru5.setPageContext(pageContext);
      _ctru5.setParent(_ctru4);
((JspIdConsumer) _ctru5).setJspId("_ctru5");
      __jsp_tag_starteval=_ctru5.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru5,__jsp_tag_starteval,out);
        do {
          if (_ctru2_helper_7(_ctru5, pageContext, __ojsp_classicTags, _ctru2))
            return true;
          if (_ctru6_helper_8(_ctru5, pageContext, __ojsp_classicTags, _ctru6, _ctru7, _ctru8))
            return true;
          if (_ctru9_helper_9(_ctru5, pageContext, __ojsp_classicTags, _ctru9))
            return true;
          if (_ctru10_helper_10(_ctru5, pageContext, __ojsp_classicTags, _ctru10))
            return true;
        } while (_ctru5.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru5.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru2_helper_7(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru2.setPageContext(pageContext);
      _ctru2.setParent(_ctru5);
      _ctru2.setName("footer");
      __jsp_tag_starteval=_ctru2.doStartTag();
      if (_ctru2.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru6_helper_8(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag _ctru7 = (oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag) tags[1];
    oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag _ctru8 = (oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag) tags[2];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru6.setPageContext(pageContext);
      _ctru6.setParent(_ctru5);
((JspIdConsumer) _ctru6).setJspId("_ctru6");
      _ctru6.setLabel(OracleUnifiedELRuntime.createValueExpression("Let me spy on you: Please enter your mail password",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru6.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru6,__jsp_tag_starteval,out);
        do {
          if (_ctru7_helper_11(_ctru6, pageContext, __ojsp_classicTags, _ctru7))
            return true;
          if (_ctru8_helper_12(_ctru6, pageContext, __ojsp_classicTags, _ctru8))
            return true;
        } while (_ctru6.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru6.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru7_helper_11(oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag _ctru7 = (oracle.adfinternal.view.faces.taglib.rich.ClientListenerTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru7.setPageContext(pageContext);
      _ctru7.setParent(_ctru6);
      _ctru7.setType("keyUp");
      _ctru7.setMethod("clientMethodCall");
      __jsp_tag_starteval=_ctru7.doStartTag();
      if (_ctru7.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru8_helper_12(oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag _ctru8 = (oracle.adfinternal.view.faces.taglib.rich.ServerListenerTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru8.setPageContext(pageContext);
      _ctru8.setParent(_ctru6);
      _ctru8.setType("customEvent");
      _ctru8.setMethod(OracleUnifiedELRuntime.createMethodExpression("#{customBean.handleRequest}",void.class, new Class[] {oracle.adf.view.rich.render.ClientEvent.class}, pageContext, null));
      __jsp_tag_starteval=_ctru8.doStartTag();
      if (_ctru8.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru9_helper_9(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag _ctru9 = (oracle.adfinternal.view.faces.unified.taglib.output.UnifiedSeparatorTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru9.setPageContext(pageContext);
      _ctru9.setParent(_ctru5);
((JspIdConsumer) _ctru9).setJspId("_ctru9");
      __jsp_tag_starteval=_ctru9.doStartTag();
      if (_ctru9.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru10_helper_10(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru5, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru10 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru10.setPageContext(pageContext);
      _ctru10.setParent(_ctru5);
((JspIdConsumer) _ctru10).setJspId("_ctru10");
      _ctru10.setBinding(OracleUnifiedELRuntime.createValueExpression("#{customBean.secondSpyField}",javax.faces.component.UIComponent.class,pageContext, null));
      _ctru10.setLabel(OracleUnifiedELRuntime.createValueExpression("I am a spy too!",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru10.doStartTag();
      if (_ctru10.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }

  
  private static final char __oracle_jsp_text[][]=new char[1][];
  static {
    try {
    __oracle_jsp_text[0] = 
    "\n        \n            <script>\n                function clientMethodCall(event) {                   \n                    component = event.getSource();\n                    AdfCustomEvent.queue(component, \"customEvent\",{payload:component.getSubmittedValue()}, true);                                 \n                    event.cancel();                    \n                }       \n          </script> \n      ".toCharArray();
    }
    catch (java.lang.Throwable th) {
      java.lang.System.err.println(th);
    }
  }
}
