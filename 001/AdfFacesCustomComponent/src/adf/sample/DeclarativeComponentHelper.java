package adf.sample;


import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.fragment.UIXDeclarativeComponent;


public class DeclarativeComponentHelper {
    public DeclarativeComponentHelper() {
    }

    public void onButtonPressed(ActionEvent actionEvent) {
        UIXDeclarativeComponent  _this = (UIXDeclarativeComponent ) getValueObject("#{component}",UIXDeclarativeComponent .class);

        String s = (String)_this.getAttributes().get("userInput");
        System.out.println("The provided value was: "+s);
    }
    
    private Object getValueObject(String expr, Class returnType){
        FacesContext fc = FacesContext.getCurrentInstance();       
        ELContext elctx  = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();       
        ValueExpression valueExpr = elFactory.createValueExpression(elctx,expr,returnType);
        return valueExpr.getValue(elctx);
    }    
}
