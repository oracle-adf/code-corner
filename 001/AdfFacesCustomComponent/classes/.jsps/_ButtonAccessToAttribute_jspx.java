
import oracle.jsp.runtime.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import oracle.jsp.el.*;
import javax.el.*;


public class _ButtonAccessToAttribute_jspx extends com.orionserver.http.OrionHttpJspPage {


  // ** Begin Declarations


  // ** End Declarations

  public void _jspService(HttpServletRequest request, HttpServletResponse response) throws java.io.IOException, ServletException {

    response.setContentType( "text/html;charset=windows-1252");
    /* set up the intrinsic variables using the pageContext goober:
    ** session = HttpSession
    ** application = ServletContext
    ** out = JspWriter
    ** page = this
    ** config = ServletConfig
    ** all session/app beans declared in globals.jsa
    */
    PageContext pageContext = JspFactory.getDefaultFactory().getPageContext( this, request, response, null, true, JspWriter.DEFAULT_BUFFER, true);
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    int __jsp_tag_starteval;
    ServletContext application = pageContext.getServletContext();
    JspWriter out = pageContext.getOut();
    _ButtonAccessToAttribute_jspx page = this;
    ServletConfig config = pageContext.getServletConfig();

    try {
      // compile time tag reuse - begin
      JspTag[] __ojsp_classicTags = new JspTag[3];
      // usage : oracle.adfinternal.view.faces.taglib.region.ComponentDefTag var componentVar
      oracle.adfinternal.view.faces.taglib.region.ComponentDefTag _ctru0 = (oracle.adfinternal.view.faces.taglib.region.ComponentDefTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.taglib.region.ComponentDefTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag actionListener text
      oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.taglib.region.XmlContentTag
      oracle.adfinternal.view.faces.taglib.region.XmlContentTag _ctru2 = (oracle.adfinternal.view.faces.taglib.region.XmlContentTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.taglib.region.XmlContentTag.class, "compiletime");
      // compile tag reuse - end


      if (_ctru0_helper_1(pageContext, __ojsp_classicTags, _ctru0, _ctru1, _ctru2))
        return;

    }
    catch (java.lang.Throwable e) {
      if (!(e instanceof javax.servlet.jsp.SkipPageException)){
        try {
          if (out != null) out.clear();
        }
        catch (java.lang.Exception clearException) {
        }
        pageContext.handlePageException(e);
      }
    }
    finally {
      OracleJspRuntime.extraHandlePCFinally(pageContext, true);
      JspFactory.getDefaultFactory().releasePageContext(pageContext);
    }

  }
    
  private boolean _ctru0_helper_1(PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.taglib.region.ComponentDefTag _ctru0 = (oracle.adfinternal.view.faces.taglib.region.ComponentDefTag) tags[0];
    oracle.adfinternal.view.faces.taglib.region.XmlContentTag _ctru2 = (oracle.adfinternal.view.faces.taglib.region.XmlContentTag) tags[2];
    oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) tags[1];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru0.setPageContext(pageContext);
      _ctru0.setParent(null);
      _ctru0.setVar("attrs");
      _ctru0.setComponentVar("component");
      __jsp_tag_starteval=_ctru0.doStartTag();
      if (OracleJspRuntime.checkStartTagEval(__jsp_tag_starteval))
      {
        do {
          if (_ctru1_helper_2(_ctru0, pageContext, __ojsp_classicTags, _ctru1))
            return true;
          if (_ctru2_helper_3(_ctru0, pageContext, __ojsp_classicTags, _ctru2))
            return true;
        } while (_ctru0.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
      }
      if (_ctru0.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru1_helper_2(oracle.adfinternal.view.faces.taglib.region.ComponentDefTag _ctru0, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru1.setPageContext(pageContext);
      _ctru1.setParent(_ctru0);
((JspIdConsumer) _ctru1).setJspId("_ctru1");
      _ctru1.setActionListener(OracleUnifiedELRuntime.createMethodExpression("#{DeclarativeComponentHelper.onButtonPressed}",void.class, new Class[] {javax.faces.event.ActionEvent.class}, pageContext, null));
      _ctru1.setText(OracleUnifiedELRuntime.createValueExpression("Press Me",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru1.doStartTag();
      if (_ctru1.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru2_helper_3(oracle.adfinternal.view.faces.taglib.region.ComponentDefTag _ctru0, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.taglib.region.XmlContentTag _ctru2 = (oracle.adfinternal.view.faces.taglib.region.XmlContentTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru2.setPageContext(pageContext);
      _ctru2.setParent(_ctru0);
      __jsp_tag_starteval=_ctru2.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru2,__jsp_tag_starteval,out);
        do {
          out.write( "<component" + " xmlns=\"" + "http://xmlns.oracle.com/adf/faces/rich/component"+ "\"" +">");
          out.write( "<display-name>");
          out.write(__oracle_jsp_text[0]);
          out.write( "</display-name>");
          out.write( "<attribute>");
          out.write( "<attribute-name>");
          out.write(__oracle_jsp_text[1]);
          out.write( "</attribute-name>");
          out.write( "<attribute-class>");
          out.write(__oracle_jsp_text[2]);
          out.write( "</attribute-class>");
          out.write( "<required>");
          out.write(__oracle_jsp_text[3]);
          out.write( "</required>");
          out.write( "</attribute>");
          out.write( "<component-extension>");
          out.write( "<component-tag-namespace>");
          out.write(__oracle_jsp_text[4]);
          out.write( "</component-tag-namespace>");
          out.write( "<component-taglib-uri>");
          out.write(__oracle_jsp_text[5]);
          out.write( "</component-taglib-uri>");
          out.write( "</component-extension>");
          out.write( "</component>");
        } while (_ctru2.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru2.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }

  
  private static final char __oracle_jsp_text[][]=new char[6][];
  static {
    try {
    __oracle_jsp_text[0] = 
    "ButtonAccessToAttribute".toCharArray();
    __oracle_jsp_text[1] = 
    "userInput".toCharArray();
    __oracle_jsp_text[2] = 
    "java.lang.String".toCharArray();
    __oracle_jsp_text[3] = 
    "true".toCharArray();
    __oracle_jsp_text[4] = 
    "component".toCharArray();
    __oracle_jsp_text[5] = 
    "/custom/buttons".toCharArray();
    }
    catch (java.lang.Throwable th) {
      java.lang.System.err.println(th);
    }
  }
}
