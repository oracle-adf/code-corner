
import oracle.jsp.runtime.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import oracle.jsp.el.*;
import javax.el.*;


public class _TestDeclarativeComponent_jspx extends com.orionserver.http.OrionHttpJspPage {


  // ** Begin Declarations


  // ** End Declarations

  public void _jspService(HttpServletRequest request, HttpServletResponse response) throws java.io.IOException, ServletException {

    response.setContentType( "text/html;charset=windows-1252");
    /* set up the intrinsic variables using the pageContext goober:
    ** session = HttpSession
    ** application = ServletContext
    ** out = JspWriter
    ** page = this
    ** config = ServletConfig
    ** all session/app beans declared in globals.jsa
    */
    PageContext pageContext = JspFactory.getDefaultFactory().getPageContext( this, request, response, null, true, JspWriter.DEFAULT_BUFFER, true);
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    int __jsp_tag_starteval;
    ServletContext application = pageContext.getServletContext();
    JspWriter out = pageContext.getOut();
    _TestDeclarativeComponent_jspx page = this;
    ServletConfig config = pageContext.getServletConfig();

    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    try {
      // compile time tag reuse - begin
      JspTag[] __ojsp_classicTags = new JspTag[8];
      // usage : com.sun.faces.taglib.jsf_core.ViewTag
      com.sun.faces.taglib.jsf_core.ViewTag _ctru0 = (com.sun.faces.taglib.jsf_core.ViewTag) OracleJspRuntime.getTagHandler(pageContext, com.sun.faces.taglib.jsf_core.ViewTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag
      oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag
      oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru2 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag
      oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag value label
      oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag id text partialSubmit
      oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag partialTriggers text
      oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag.class, "compiletime");
      // usage : component.ButtonAccessToAttributeTag userInput
      component.ButtonAccessToAttributeTag _ctru7 = (component.ButtonAccessToAttributeTag) OracleJspRuntime.getTagHandler(pageContext, component.ButtonAccessToAttributeTag.class, "compiletime");
      // compile tag reuse - end


      if (_ctru0_helper_1(pageContext, __ojsp_classicTags, _ctru0, _ctru1, _ctru2, _ctru3, _ctru4, _ctru5, _ctru6, _ctru7))
        return;

    }
    catch (java.lang.Throwable e) {
      if (!(e instanceof javax.servlet.jsp.SkipPageException)){
        try {
          if (out != null) out.clear();
        }
        catch (java.lang.Exception clearException) {
        }
        pageContext.handlePageException(e);
      }
    }
    finally {
      OracleJspRuntime.extraHandlePCFinally(pageContext, true);
      JspFactory.getDefaultFactory().releasePageContext(pageContext);
    }

  }
    
  private boolean _ctru0_helper_1(PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    com.sun.faces.taglib.jsf_core.ViewTag _ctru0 = (com.sun.faces.taglib.jsf_core.ViewTag) tags[0];
    component.ButtonAccessToAttributeTag _ctru7 = (component.ButtonAccessToAttributeTag) tags[7];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru2 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag) tags[2];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[4];
    oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag) tags[6];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[3];
    oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) tags[5];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    int __jsp_tag_starteval;

    {
      _ctru0.setPageContext(pageContext);
      _ctru0.setParent(null);
((JspIdConsumer) _ctru0).setJspId("_ctru0");
      __jsp_tag_starteval=_ctru0.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru0,__jsp_tag_starteval,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
        do {
          if (_ctru1_helper_2(_ctru0, pageContext, __ojsp_classicTags, _ctru1, _ctru2, _ctru3, _ctru4, _ctru5, _ctru6, _ctru7))
            return true;
        } while (_ctru0.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
      }
      if (_ctru0.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru1_helper_2(com.sun.faces.taglib.jsf_core.ViewTag _ctru0, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    component.ButtonAccessToAttributeTag _ctru7 = (component.ButtonAccessToAttributeTag) tags[6];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag) tags[0];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru2 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[3];
    oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag) tags[5];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[2];
    oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) tags[4];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    int __jsp_tag_starteval;

    {
      _ctru1.setPageContext(pageContext);
      _ctru1.setParent(_ctru0);
((JspIdConsumer) _ctru1).setJspId("_ctru1");
      __jsp_tag_starteval=_ctru1.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru1,__jsp_tag_starteval,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
        do {
          if (_ctru2_helper_3(_ctru1, pageContext, __ojsp_classicTags, _ctru2, _ctru3, _ctru4, _ctru5, _ctru6, _ctru7))
            return true;
        } while (_ctru1.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
      }
      if (_ctru1.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru2_helper_3(oracle.adfinternal.view.faces.unified.taglib.UnifiedDocumentTag _ctru1, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    component.ButtonAccessToAttributeTag _ctru7 = (component.ButtonAccessToAttributeTag) tags[5];
    oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru2 = (oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag) tags[0];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[2];
    oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag) tags[4];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) tags[3];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    int __jsp_tag_starteval;

    {
      _ctru2.setPageContext(pageContext);
      _ctru2.setParent(_ctru1);
((JspIdConsumer) _ctru2).setJspId("_ctru2");
      __jsp_tag_starteval=_ctru2.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru2,__jsp_tag_starteval,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
        do {
          if (_ctru3_helper_4(_ctru2, pageContext, __ojsp_classicTags, _ctru3, _ctru4, _ctru5))
            return true;
          if (_ctru6_helper_5(_ctru2, pageContext, __ojsp_classicTags, _ctru6, _ctru7))
            return true;
        } while (_ctru2.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
      }
      if (_ctru2.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru3_helper_4(oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru2, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[0];
    oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) tags[2];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    int __jsp_tag_starteval;

    {
      _ctru3.setPageContext(pageContext);
      _ctru3.setParent(_ctru2);
((JspIdConsumer) _ctru3).setJspId("_ctru3");
      __jsp_tag_starteval=_ctru3.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru3,__jsp_tag_starteval,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
        do {
          if (_ctru4_helper_6(_ctru3, pageContext, __ojsp_classicTags, _ctru4))
            return true;
          if (_ctru5_helper_7(_ctru3, pageContext, __ojsp_classicTags, _ctru5))
            return true;
        } while (_ctru3.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
      }
      if (_ctru3.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru4_helper_6(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    int __jsp_tag_starteval;

    {
      _ctru4.setPageContext(pageContext);
      _ctru4.setParent(_ctru3);
((JspIdConsumer) _ctru4).setJspId("_ctru4");
      _ctru4.setValue(OracleUnifiedELRuntime.createValueExpression("#{pageFlowScope.userInput}",java.lang.Object.class,pageContext, null));
      _ctru4.setLabel(OracleUnifiedELRuntime.createValueExpression("Input",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru4.doStartTag();
      if (_ctru4.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru5_helper_7(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.nav.UnifiedCommandButtonTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    int __jsp_tag_starteval;

    {
      _ctru5.setPageContext(pageContext);
      _ctru5.setParent(_ctru3);
((JspIdConsumer) _ctru5).setJspId("_ctru5");
      _ctru5.setId("cmdButtonId");
      _ctru5.setText(OracleUnifiedELRuntime.createValueExpression("Set Value to Pass",java.lang.Object.class,pageContext, null));
      _ctru5.setPartialSubmit(OracleUnifiedELRuntime.createValueExpression("true",boolean.class,pageContext, null));
      __jsp_tag_starteval=_ctru5.doStartTag();
      if (_ctru5.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru6_helper_5(oracle.adfinternal.view.faces.unified.taglib.UnifiedFormTag _ctru2, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    component.ButtonAccessToAttributeTag _ctru7 = (component.ButtonAccessToAttributeTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    int __jsp_tag_starteval;

    {
      _ctru6.setPageContext(pageContext);
      _ctru6.setParent(_ctru2);
((JspIdConsumer) _ctru6).setJspId("_ctru6");
      _ctru6.setPartialTriggers(OracleUnifiedELRuntime.createValueExpression("cmdButtonId",java.lang.Object.class,pageContext, null));
      _ctru6.setText(OracleUnifiedELRuntime.createValueExpression("Custom Component - Press Me",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru6.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru6,__jsp_tag_starteval,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
        do {
          if (_ctru7_helper_8(_ctru6, pageContext, __ojsp_classicTags, _ctru7))
            return true;
        } while (_ctru6.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
        __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
      }
      if (_ctru6.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru7_helper_8(oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru6, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    component.ButtonAccessToAttributeTag _ctru7 = (component.ButtonAccessToAttributeTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    com.evermind.server.http.JspCommonExtraWriter __ojsp_s_out = (com.evermind.server.http.JspCommonExtraWriter) out;
    int __jsp_tag_starteval;

    {
      _ctru7.setPageContext(pageContext);
      _ctru7.setParent(_ctru6);
((JspIdConsumer) _ctru7).setJspId("_ctru7");
      _ctru7.setUserInput(OracleUnifiedELRuntime.createValueExpression("#{pageFlowScope.userInput}",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru7.doStartTag();
      if (_ctru7.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }

}
