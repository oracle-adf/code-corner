
import oracle.jsp.runtime.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import oracle.jsp.el.*;
import javax.el.*;


public class _UserInfo_jspx extends com.orionserver.http.OrionHttpJspPage {


  // ** Begin Declarations


  // ** End Declarations

  public void _jspService(HttpServletRequest request, HttpServletResponse response) throws java.io.IOException, ServletException {

    response.setContentType( "text/html;charset=windows-1252");
    /* set up the intrinsic variables using the pageContext goober:
    ** session = HttpSession
    ** application = ServletContext
    ** out = JspWriter
    ** page = this
    ** config = ServletConfig
    ** all session/app beans declared in globals.jsa
    */
    PageContext pageContext = JspFactory.getDefaultFactory().getPageContext( this, request, response, null, true, JspWriter.DEFAULT_BUFFER, true);
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    int __jsp_tag_starteval;
    ServletContext application = pageContext.getServletContext();
    JspWriter out = pageContext.getOut();
    _UserInfo_jspx page = this;
    ServletConfig config = pageContext.getServletConfig();

    try {
      // compile time tag reuse - begin
      JspTag[] __ojsp_classicTags = new JspTag[8];
      // usage : oracle.adfinternal.view.faces.taglib.region.ComponentDefTag var componentVar
      oracle.adfinternal.view.faces.taglib.region.ComponentDefTag _ctru0 = (oracle.adfinternal.view.faces.taglib.region.ComponentDefTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.taglib.region.ComponentDefTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag text
      oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag.class, "compiletime");
      // usage : javax.faces.webapp.FacetTag name
      javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) OracleJspRuntime.getTagHandler(pageContext, javax.faces.webapp.FacetTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag
      oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag value label
      oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag value label
      oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag value label
      oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag.class, "compiletime");
      // usage : oracle.adfinternal.view.faces.taglib.region.XmlContentTag
      oracle.adfinternal.view.faces.taglib.region.XmlContentTag _ctru7 = (oracle.adfinternal.view.faces.taglib.region.XmlContentTag) OracleJspRuntime.getTagHandler(pageContext, oracle.adfinternal.view.faces.taglib.region.XmlContentTag.class, "compiletime");
      // compile tag reuse - end


      if (_ctru0_helper_1(pageContext, __ojsp_classicTags, _ctru0, _ctru1, _ctru2, _ctru3, _ctru2, _ctru4, _ctru5, _ctru6, _ctru7))
        return;

    }
    catch (java.lang.Throwable e) {
      if (!(e instanceof javax.servlet.jsp.SkipPageException)){
        try {
          if (out != null) out.clear();
        }
        catch (java.lang.Exception clearException) {
        }
        pageContext.handlePageException(e);
      }
    }
    finally {
      OracleJspRuntime.extraHandlePCFinally(pageContext, true);
      JspFactory.getDefaultFactory().releasePageContext(pageContext);
    }

  }
    
  private boolean _ctru0_helper_1(PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[2];
    oracle.adfinternal.view.faces.taglib.region.ComponentDefTag _ctru0 = (oracle.adfinternal.view.faces.taglib.region.ComponentDefTag) tags[0];
    oracle.adfinternal.view.faces.taglib.region.XmlContentTag _ctru7 = (oracle.adfinternal.view.faces.taglib.region.XmlContentTag) tags[8];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[5];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[6];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[7];
    oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[3];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru0.setPageContext(pageContext);
      _ctru0.setParent(null);
      _ctru0.setVar("attrs");
      _ctru0.setComponentVar("component");
      __jsp_tag_starteval=_ctru0.doStartTag();
      if (OracleJspRuntime.checkStartTagEval(__jsp_tag_starteval))
      {
        do {
          if (_ctru1_helper_2(_ctru0, pageContext, __ojsp_classicTags, _ctru1, _ctru2, _ctru3, _ctru2, _ctru4, _ctru5, _ctru6))
            return true;
          if (_ctru7_helper_3(_ctru0, pageContext, __ojsp_classicTags, _ctru7))
            return true;
        } while (_ctru0.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
      }
      if (_ctru0.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru1_helper_2(oracle.adfinternal.view.faces.taglib.region.ComponentDefTag _ctru0, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[4];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[5];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[6];
    oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru1 = (oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag) tags[0];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[2];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru1.setPageContext(pageContext);
      _ctru1.setParent(_ctru0);
((JspIdConsumer) _ctru1).setJspId("_ctru1");
      _ctru1.setText(OracleUnifiedELRuntime.createValueExpression("User Info",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru1.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru1,__jsp_tag_starteval,out);
        do {
          if (_ctru2_helper_4(_ctru1, pageContext, __ojsp_classicTags, _ctru2))
            return true;
          if (_ctru3_helper_5(_ctru1, pageContext, __ojsp_classicTags, _ctru3, _ctru2, _ctru4, _ctru5, _ctru6))
            return true;
        } while (_ctru1.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru1.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru2_helper_4(oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru1, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru2.setPageContext(pageContext);
      _ctru2.setParent(_ctru1);
      _ctru2.setName("toolbar");
      __jsp_tag_starteval=_ctru2.doStartTag();
      if (_ctru2.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru3_helper_5(oracle.adfinternal.view.faces.unified.taglib.layout.CorePanelBoxTag _ctru1, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[1];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[2];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[3];
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[4];
    oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3 = (oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru3.setPageContext(pageContext);
      _ctru3.setParent(_ctru1);
((JspIdConsumer) _ctru3).setJspId("_ctru3");
      __jsp_tag_starteval=_ctru3.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru3,__jsp_tag_starteval,out);
        do {
          if (_ctru2_helper_6(_ctru3, pageContext, __ojsp_classicTags, _ctru2))
            return true;
          if (_ctru4_helper_7(_ctru3, pageContext, __ojsp_classicTags, _ctru4))
            return true;
          if (_ctru5_helper_8(_ctru3, pageContext, __ojsp_classicTags, _ctru5))
            return true;
          if (_ctru6_helper_9(_ctru3, pageContext, __ojsp_classicTags, _ctru6))
            return true;
        } while (_ctru3.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru3.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru2_helper_6(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    javax.faces.webapp.FacetTag _ctru2 = (javax.faces.webapp.FacetTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru2.setPageContext(pageContext);
      _ctru2.setParent(_ctru3);
      _ctru2.setName("footer");
      __jsp_tag_starteval=_ctru2.doStartTag();
      if (_ctru2.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru4_helper_7(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru4 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru4.setPageContext(pageContext);
      _ctru4.setParent(_ctru3);
((JspIdConsumer) _ctru4).setJspId("_ctru4");
      _ctru4.setValue(OracleUnifiedELRuntime.createValueExpression("#{attrs.FirstName}",java.lang.Object.class,pageContext, null));
      _ctru4.setLabel(OracleUnifiedELRuntime.createValueExpression("First Name",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru4.doStartTag();
      if (_ctru4.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru5_helper_8(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru5 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru5.setPageContext(pageContext);
      _ctru5.setParent(_ctru3);
((JspIdConsumer) _ctru5).setJspId("_ctru5");
      _ctru5.setValue(OracleUnifiedELRuntime.createValueExpression("#{attrs.LastName}",java.lang.Object.class,pageContext, null));
      _ctru5.setLabel(OracleUnifiedELRuntime.createValueExpression("Lastname",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru5.doStartTag();
      if (_ctru5.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru6_helper_9(oracle.adfinternal.view.faces.unified.taglib.layout.UnifiedPanelFormLayoutTag _ctru3, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag _ctru6 = (oracle.adfinternal.view.faces.unified.taglib.input.UnifiedInputTextTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru6.setPageContext(pageContext);
      _ctru6.setParent(_ctru3);
((JspIdConsumer) _ctru6).setJspId("_ctru6");
      _ctru6.setValue(OracleUnifiedELRuntime.createValueExpression("#{attrs.Mail}",java.lang.Object.class,pageContext, null));
      _ctru6.setLabel(OracleUnifiedELRuntime.createValueExpression("Mail",java.lang.Object.class,pageContext, null));
      __jsp_tag_starteval=_ctru6.doStartTag();
      if (_ctru6.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }
  
  private boolean _ctru7_helper_3(oracle.adfinternal.view.faces.taglib.region.ComponentDefTag _ctru0, PageContext pageContext, JspTag[] __ojsp_classicTags, Object... tags  ) throws  java.lang.Throwable {
    
    oracle.adfinternal.view.faces.taglib.region.XmlContentTag _ctru7 = (oracle.adfinternal.view.faces.taglib.region.XmlContentTag) tags[0];
    
    // Note: this is not emitted if the session directive == false
    HttpSession session = pageContext.getSession();
    JspWriter out = pageContext.getOut();
    int __jsp_tag_starteval;

    {
      _ctru7.setPageContext(pageContext);
      _ctru7.setParent(_ctru0);
      __jsp_tag_starteval=_ctru7.doStartTag();
      if (OracleJspRuntime.checkStartBodyTagEval(__jsp_tag_starteval))
      {
        out=OracleJspRuntime.pushBodyIfNeeded(pageContext,_ctru7,__jsp_tag_starteval,out);
        do {
          out.write( "<component" + " xmlns=\"" + "http://xmlns.oracle.com/adf/faces/rich/component"+ "\"" +">");
          out.write( "<display-name>");
          out.write(__oracle_jsp_text[0]);
          out.write( "</display-name>");
          out.write( "<attribute>");
          out.write( "<attribute-name>");
          out.write(__oracle_jsp_text[1]);
          out.write( "</attribute-name>");
          out.write( "<attribute-class>");
          out.write(__oracle_jsp_text[2]);
          out.write( "</attribute-class>");
          out.write( "</attribute>");
          out.write( "<attribute>");
          out.write( "<attribute-name>");
          out.write(__oracle_jsp_text[3]);
          out.write( "</attribute-name>");
          out.write( "<attribute-class>");
          out.write(__oracle_jsp_text[4]);
          out.write( "</attribute-class>");
          out.write( "</attribute>");
          out.write( "<attribute>");
          out.write( "<attribute-name>");
          out.write(__oracle_jsp_text[5]);
          out.write( "</attribute-name>");
          out.write( "<attribute-class>");
          out.write(__oracle_jsp_text[6]);
          out.write( "</attribute-class>");
          out.write( "</attribute>");
          out.write( "<component-extension>");
          out.write( "<component-tag-namespace>");
          out.write(__oracle_jsp_text[7]);
          out.write( "</component-tag-namespace>");
          out.write( "<component-taglib-uri>");
          out.write(__oracle_jsp_text[8]);
          out.write( "</component-taglib-uri>");
          out.write( "</component-extension>");
          out.write( "</component>");
        } while (_ctru7.doAfterBody()==BodyTag.EVAL_BODY_AGAIN);
        out=OracleJspRuntime.popBodyIfNeeded(pageContext,out);
      }
      if (_ctru7.doEndTag()==Tag.SKIP_PAGE)
        return true;
    }
    return false;
  }

  
  private static final char __oracle_jsp_text[][]=new char[9][];
  static {
    try {
    __oracle_jsp_text[0] = 
    "UserInfo".toCharArray();
    __oracle_jsp_text[1] = 
    "\n            FirstName\n          ".toCharArray();
    __oracle_jsp_text[2] = 
    "\n            java.lang.String\n          ".toCharArray();
    __oracle_jsp_text[3] = 
    "\n            LastName\n          ".toCharArray();
    __oracle_jsp_text[4] = 
    "\n            java.lang.String\n          ".toCharArray();
    __oracle_jsp_text[5] = 
    "\n            Mail\n          ".toCharArray();
    __oracle_jsp_text[6] = 
    "\n            java.lang.String\n          ".toCharArray();
    __oracle_jsp_text[7] = 
    "adf.custom.comp".toCharArray();
    __oracle_jsp_text[8] = 
    "/fnimphiu".toCharArray();
    }
    catch (java.lang.Throwable th) {
      java.lang.System.err.println(th);
    }
  }
}
