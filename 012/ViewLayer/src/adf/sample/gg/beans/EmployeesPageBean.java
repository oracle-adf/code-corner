package adf.sample.gg.beans;

import java.util.Iterator;
import java.util.List;
import oracle.adf.model.BindingContext;
import oracle.adf.model.bean.DCDataRow;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.render.ClientEvent;
import oracle.binding.OperationBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;
import org.apache.myfaces.trinidad.model.RowKeySet;

/**
 * Bean that handles the POJO model update
 */
public class EmployeesPageBean {
    
    private RichTable employeeTable1;

    public EmployeesPageBean() {
    }
   
   public void setEmployeeTable1(RichTable employeeTable1) {
       this.employeeTable1 = employeeTable1;
   }

   public RichTable getEmployeeTable1() {
       return employeeTable1;
   }    
  
   /**
     * Method called from the serverListener on the page. The values that are passed
     * by the ClientEvent object is customizable and in this sample include the row
     * index and the cell attribute name to copy and paste values from. You could also
     * pass the component clientId so that the managed bean e.g. could set the focus 
     * back if needed
     * @param ce
     */
   public void copyValueToSelectedRows(ClientEvent ce){
      String columnToUpdate = (String) ce.getParameters().get("column");    
      RichTable table = this.getEmployeeTable1();
      Object oldKey = table.getRowKey();
        
      try {
           int rowKeyIndex =
           ((Double)ce.getParameters().get("rowKeyIndex")).intValue();
           updateSelectedTableRows(columnToUpdate, rowKeyIndex);
      }
      catch(Exception e){
        e.printStackTrace();
      }
      finally {
         table.setRowKey(oldKey);
      }  
  }
   
  /**
     * Method that identifies the table row to copy the cell value from to then copy 
     * the value to all selected table rows. Optionally, at the end of ths method you
     * then update the POJO model (which is an action not required if you use ADF BC 
     * because the binding layer is updated and within the next submit automatically 
     * updates the ADF BC model. However, this sample uses a POJO model and as such
     * we perform the update
     * @param column          The attribute name to copy the values from and paste them to
     * @param rowKeyIndex     the row index of the ADF binding row to copy from. This is used
     *                        if you want to copy the values from the ADF binding layer and not
     *                        use the value from the browser client. Its also the default in this
     *                        sample.
     */
  private void updateSelectedTableRows(String column, int rowKeyIndex){
      
      RichTable table = this.getEmployeeTable1();
      
      /*
       * 1. Get access to the copied cell data. This information can be accessed directly 
       *    on the ADF Faces table or through the binding layer. 
       */
      
      //set current table row to copy source
      table.setRowIndex(rowKeyIndex);
      JUCtrlHierNodeBinding rowBinding = (JUCtrlHierNodeBinding) table.getRowData();
      
      /* DCDataRow extends ViewRowImpl, which extends RowImpl that is used with ADF 
       * BC View Objects and Entity Objects. This means that DCDataRow is the class
       * to use if your code needs to run with ADF BC and non-ADF BC business services
       */
      DCDataRow rowToCopyFrom = (DCDataRow) rowBinding.getRow();          
      Object copyValue = rowToCopyFrom.getAttribute(column);
      
      //* END OF OPTIONAl    
          
      /*
       * 2. Copy the data to the selected table rows. Here we have two options: paste
       *    the value to the ADF Faces table or access the ADF binding layer (iterator).
       */
      
      RowKeySet selectedRowKeySet = table.getSelectedRowKeys();
      Iterator selectedRowKeySetIter = selectedRowKeySet.iterator();
      
      while (selectedRowKeySetIter.hasNext()){
          List key = (List) selectedRowKeySetIter.next();
          //make row current in table
          table.setRowKey(key);      
          JUCtrlHierNodeBinding selectedRowBinding = (JUCtrlHierNodeBinding) table.getRowData();
          DCDataRow rowToUpdate = (DCDataRow) selectedRowBinding.getRow();
          
          //copy the value from one attribute to the current selected
          rowToUpdate.setAttribute(column, copyValue);
          
          /*
           * 3. So far we did update the ADF iterator, which is sufficient if you 
           *    use ADF BC because of its active data model. Other business services
           *    expose methods to persist changes, which needs to be explicitly called.
           *    
           *    Note that you can only persist rows that are complete and don't miss 
           *    required values. In this sample, we update existing rows only so that 
           *    it is safe to update. 
           */
          BindingContext bctx = BindingContext.getCurrent();
          DCBindingContainer bindings = (DCBindingContainer) bctx.getCurrentBindingsEntry();
          
          //method to update the POJO model. Note that for Pojo, EJB and WS models, 
          //you need to explicity calls a method to merge (update) or persist (commit)
          //objects
          OperationBinding mergeOperation = bindings.getOperationBinding("mergeEmployee");
          
          //Since we use POJOs we need to pass the "real" object, which we get from the 
          //DataProvider of the row. The same is required for Web Services, EJB/JPA etc. 
          mergeOperation.getParamsMap().put("emp", rowToUpdate.getDataProvider());
          //execute the method for update
          mergeOperation.execute();
          //any errors ?
          if(mergeOperation.getErrors().size() > 0){
            //print first error message
            System.out.println("An Error occured when updating row: " +
                                mergeOperation.getErrors().get(0));
          }
        }      
      //PPR the table to display copied values
      AdfFacesContext adffctx = AdfFacesContext.getCurrentInstance();
      adffctx.addPartialTarget(table);
    }
}
