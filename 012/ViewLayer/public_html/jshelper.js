
/*
 * A global variable that holds the cell handler of the table cell that 
 * we want to copy to oter rows
 */
var globalFieldCopy = null;
var globalLastVisitedField = null;

/*
 * Function is called to open a context menu dialog next to the 
 * selected table cell. It also saves the selected text field 
 * component reference for later use when pasting the value to 
 * selected table rows
 */
function openPopup(popupId){
  return function(evt){
    evt.cancel();    
    //get the field reference to copy value from
    
    txtField = evt.getSource();
    //just temporarily remember the field that had focus 
    //when the popup menu opened. This is to ensure that
    //fields are only copied when the Copy context menu 
    //option is used
    
    globalLastVisitedField = txtField;
    
    //the context popup menu should be launched in the table next
    //to the selected table cell. For this we need to get the cell
    //handler component's clientId
    var clientId = txtField.getClientId();
    //search popup from page root
    var popup = AdfPage.PAGE.findComponentByAbsoluteId(popupId);
    //align popup so it renders after the textfield
    var hints = {align:"end_before", alignId:clientId}; 
    popup.show(hints);  
  }
}

/*
 * As an alternative - and probably more convenient option to copy and paste
 * cell values to other rows - we provide a keyboard option to use instead of
 * the context menu. 
 * 
 * ctrl+shift+c copies the field to copy te value from. 
 * ctrl+shift+v copies the saved value to all selected fields
 * 
 * The serverListenerId argument is the name of the af:serverListener component. The
 * serverListenerOwnerComponentId is the ID of the UI component that has the server listener
 * defined as a child component. 
 */
function copyFromKeyboard(serverListenerId, serverListenerOwnerComponentId){
  return function(evt){
    
     //the keyboard event gives us the keycode and the modifier keys the 
     // user pressed
     var keyPressed = evt.getKeyCode();
     var modifier = evt.getKeyModifiers();
     
     //copy if ctrl+shift key is pressed together with the c-key
     var shiftCtrlKeyPressed = AdfKeyStroke.SHIFT_MASK | AdfKeyStroke.CTRL_MASK;
     if (modifier == shiftCtrlKeyPressed){
       if(keyPressed == AdfKeyStroke.C_KEY){         
         //copy the selected field to paste values from
         globalFieldCopy= evt.getSource();         
         //cancel keyboard event
         evt.cancel();
       }
       //paste
       else if(keyPressed == AdfKeyStroke.V_KEY){
         if(globalFieldCopy == null){
           //no value copied
           alert("No value copied. Please copy value first: Use ctrl+shift+c");
           evt.cancel();
         }
         else{
           //handle paste
           copyValueToSelectedRows(serverListenerId, serverListenerOwnerComponentId);
           evt.cancel();
         }
       }
     }     
  }
}
//Function referenced from the clientListener on the copy menu option
function copyFromMenu(evt){
 //copy the  last visited field to the variable used to copy values from. This
 //step is required to ensure the field is available when copy is invoked from
 //popup
 if(globalLastVisitedField != null){
    globalFieldCopy = globalLastVisitedField;
 }
 else{
  alert("Problem: No field could be identified to be in focus");
 }
 //cancel keyboard event
 evt.cancel();
}

//Function referenced from the clientListener on the copy menu option
function pasteFromMenu(serverListenerId, serverListenerOwnerComponentId){
  return function(evt){
    if(globalFieldCopy == null){
      //no value copied
      alert("No value copied. Please copy value first");
      evt.cancel();
    }
    else{
      //handle paste
      copyValueToSelectedRows(serverListenerId, serverListenerOwnerComponentId);
      evt.cancel();
    }
  }
}

/*
 * Function that queues a custom event to be handled by a managed bean on the server
 * side. Note that having the copy and paste action handled on the server is more
 * robust than doing the same on the client. 
 */
function copyValueToSelectedRows(serverListenerId, serverListenerOwnerComponentId) {   
    var txtField = globalFieldCopy;    
    //get the name ofthe column which row cell to read and write to
    var column = txtField.getProperty('colname');    
    //get the index of the row to copy values from
    var rowKeyIndx   = txtField.getProperty('rwKeyIndx');  
    
    var submittedValue = txtField.getSubmittedValue();
    var serverListenerHolder = AdfPage.PAGE.findComponentByAbsoluteId(serverListenerOwnerComponentId);
    
    AdfCustomEvent.queue(
       //reference the component that has the server listener 
       //defined.
       serverListenerHolder,
       //specify server listener to invoke
       serverListenerId,
       // Send two parameters. The format of this message is
       //a JSON map, which on the server side Java code becomes
       //a java.util.Map object
       {column:column, fieldValue:submittedValue, rowKeyIndex:rowKeyIndx},
       // Make it "immediate" on the server
       true);
       
       //reset field value
       globalFieldCopy = null; 
}



