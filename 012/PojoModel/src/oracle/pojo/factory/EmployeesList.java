package oracle.pojo.factory;

import java.io.Serializable;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import oracle.pojo.entities.Employees;


public class EmployeesList implements Serializable {
    ArrayList<Employees> employeesList = null;

    public EmployeesList() {
    }

 
    public List<Employees> getEmployeesList() {

        employeesList = new ArrayList<Employees>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
        //populate employees
        try {
            employeesList.add(new Employees(new Long(200), "Jennifer",
                                            "Whalen", "JWHALEN",
                                            "515.123.4444",
                                            sdf.parse("17-SEP-87"), "AD_ASST",
                                            new Long(4400), new Long(101),
                                            new Long(10)));
            employeesList.add(new Employees(new Long(201), "Michael",
                                            "Hartsteins", "MHARTSTE",
                                            "515.123.5555",
                                            sdf.parse("17-FEB-96"), "MK_MAN",
                                            new Long(13000), new Long(100),
                                            new Long(20)));
            employeesList.add(new Employees(new Long(202), "Pat", "Fay",
                                            "PFAY", "603.123.6666",
                                            sdf.parse("17-AUG-97"), "MK_REP",
                                            new Long(6000), new Long(201),
                                            new Long(20)));
            employeesList.add(new Employees(new Long(203), "Susan", "Mavris",
                                            "SMAVRIS", "515.123.7777",
                                            sdf.parse("07-JUN-94"), "HR_REP",
                                            new Long(6500), new Long(101),
                                            new Long(30)));
            employeesList.add(new Employees(new Long(114), "Den", "Raphaely",
                                            "DRAPHEAL", "515.127.4561",
                                            sdf.parse("07-DEC-94"), "PU_MAN",
                                            new Long(11000), new Long(100),
                                            new Long(30)));
            employeesList.add(new Employees(new Long(115), "Alexander", "Khoo",
                                            "AKHOO", "515.127.4562",
                                            sdf.parse("18-MAY-95"), "PU_CLERK",
                                            new Long(3100), new Long(114),
                                            new Long(30)));
            employeesList.add(new Employees(new Long(116), "Shelli", "Baida",
                                            "SBAIDA", "515.127.4563",
                                            sdf.parse("24-DEC-97"), "PU_CLERK",
                                            new Long(2900), new Long(114),
                                            new Long(30)));
            employeesList.add(new Employees(new Long(117), "Sigal", "Tobias",
                                            "STOBIAS", "515.127.4564",
                                            sdf.parse("24-JUL-97"), "PU_CLERK",
                                            new Long(2800), new Long(114),
                                            new Long(30)));
            employeesList.add(new Employees(new Long(118), "Guy", "Himuro",
                                            "GHIMURO", "515.127.4565",
                                            sdf.parse("15-NOV-98"), "PU_CLERK",
                                            new Long(2600), new Long(114),
                                            new Long(30)));
            employeesList.add(new Employees(new Long(119), "Karen",
                                            "Colmenares", "KCOLMENA",
                                            "515.127.4566",
                                            sdf.parse("10-AUG-99"), "PU_CLERK",
                                            new Long(2500), new Long(114),
                                            new Long(30)));
            employeesList.add(new Employees(new Long(198), "Donald",
                                            "OConnell", "DOCONNEL",
                                            "650.507.9833",
                                            sdf.parse("21-JUN-99"), "SH_CLERK",
                                            new Long(3000), new Long(124),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(199), "Douglas", "Grant",
                                            "DGRANT", "650.507.9844",
                                            sdf.parse("13-JAN-00"), "SH_CLERK",
                                            new Long(2600), new Long(124),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(120), "Matthew", "Weiss",
                                            "MWEISS", "650.123.1234",
                                            sdf.parse("18-JUL-96"), "ST_MAN",
                                            new Long(8000), new Long(100),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(121), "Adam", "Fripp",
                                            "AFRIPP", "650.123.2234",
                                            sdf.parse("10-APR-97"), "ST_MAN",
                                            new Long(8200), new Long(100),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(122), "Payam", "Kaufling",
                                            "PKAUFLIN", "650.123.3234",
                                            sdf.parse("01-MAY-95"), "ST_MAN",
                                            new Long(7900), new Long(100),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(123), "Shanta", "Vollman",
                                            "SVOLLMAN", "650.123.4234",
                                            sdf.parse("10-OCT-97"), "ST_MAN",
                                            new Long(6500), new Long(100),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(124), "Kevin", "Mourgos",
                                            "KMOURGOS", "650.123.5234",
                                            sdf.parse("16-NOV-99"), "ST_MAN",
                                            new Long(5800), new Long(100),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(125), "Julia", "Nayer",
                                            "JNAYER", "650.124.1214",
                                            sdf.parse("16-JUL-97"), "ST_CLERK",
                                            new Long(3200), new Long(120),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(126), "Irene",
                                            "Mikkilineni", "IMIKKILI",
                                            "650.124.1224",
                                            sdf.parse("28-SEP-98"), "ST_CLERK",
                                            new Long(2700), new Long(120),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(127), "James", "Landry",
                                            "JLANDRY", "650.124.1334",
                                            sdf.parse("14-JAN-99"), "ST_CLERK",
                                            new Long(2400), new Long(120),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(128), "Steven", "Markle",
                                            "SMARKLE", "650.124.1434",
                                            sdf.parse("08-MAR-00"), "ST_CLERK",
                                            new Long(2200), new Long(120),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(129), "Laura", "Bissot",
                                            "LBISSOT", "650.124.5234",
                                            sdf.parse("20-AUG-97"), "ST_CLERK",
                                            new Long(3300), new Long(121),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(130), "Mozhe", "Atkinson",
                                            "MATKINSO", "650.124.6234",
                                            sdf.parse("30-OCT-97"), "ST_CLERK",
                                            new Long(2800), new Long(121),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(131), "James", "Marlow",
                                            "JAMRLOW", "650.124.7234",
                                            sdf.parse("16-FEB-97"), "ST_CLERK",
                                            new Long(2500), new Long(121),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(132), "TJ", "Olson",
                                            "TJOLSON", "650.124.8234",
                                            sdf.parse("10-APR-99"), "ST_CLERK",
                                            new Long(2100), new Long(121),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(133), "Jason", "Mallin",
                                            "JMALLIN", "650.127.1934",
                                            sdf.parse("14-JUN-96"), "ST_CLERK",
                                            new Long(3300), new Long(122),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(134), "Michael", "Rogers",
                                            "MROGERS", "650.127.1834",
                                            sdf.parse("26-AUG-98"), "ST_CLERK",
                                            new Long(2900), new Long(122),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(135), "Ki", "Gee", "KGEE",
                                            "650.127.1734",
                                            sdf.parse("12-DEC-99"), "ST_CLERK",
                                            new Long(2400), new Long(122),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(136), "Hazel",
                                            "Philtanker", "HPHILTAN",
                                            "650.127.1634",
                                            sdf.parse("06-FEB-00"), "ST_CLERK",
                                            new Long(2200), new Long(122),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(137), "Renske", "Ladwig",
                                            "RLADWIG", "650.121.1234",
                                            sdf.parse("14-JUL-95"), "ST_CLERK",
                                            new Long(3600), new Long(123),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(138), "Stephen", "Stiles",
                                            "SSTILES", "650.121.2034",
                                            sdf.parse("26-OCT-97"), "ST_CLERK",
                                            new Long(3200), new Long(123),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(139), "John", "Seo",
                                            "JSEO", "650.121.2019",
                                            sdf.parse("12-FEB-98"), "ST_CLERK",
                                            new Long(2700), new Long(123),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(140), "Joshua", "Patel",
                                            "JPATEL", "650.121.1834",
                                            sdf.parse("06-APR-98"), "ST_CLERK",
                                            new Long(2500), new Long(123),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(141), "Trenna", "Rajs",
                                            "TRAJS", "650.121.8009",
                                            sdf.parse("17-OCT-95"), "ST_CLERK",
                                            new Long(3500), new Long(124),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(142), "Curtis", "Davies",
                                            "CDAVIES", "650.121.2994",
                                            sdf.parse("29-JAN-97"), "ST_CLERK",
                                            new Long(3100), new Long(124),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(143), "Randall", "Matos",
                                            "RMATOS", "650.121.2874",
                                            sdf.parse("15-MAR-98"), "ST_CLERK",
                                            new Long(2600), new Long(124),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(144), "Peter", "Vargas",
                                            "PVARGAS", "650.121.2004",
                                            sdf.parse("09-JUL-98"), "ST_CLERK",
                                            new Long(2500), new Long(124),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(180), "Winston", "Taylor",
                                            "WTAYLOR", "650.507.9876",
                                            sdf.parse("24-JAN-98"), "SH_CLERK",
                                            new Long(3200), new Long(120),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(181), "Jean", "Fleaur",
                                            "JFLEAUR", "650.507.9877",
                                            sdf.parse("23-FEB-98"), "SH_CLERK",
                                            new Long(3100), new Long(120),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(182), "Martha",
                                            "Sullivan", "MSULLIVA",
                                            "650.507.9878",
                                            sdf.parse("21-JUN-99"), "SH_CLERK",
                                            new Long(2500), new Long(120),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(183), "Girard", "Geoni",
                                            "GGEONI", "650.507.9879",
                                            sdf.parse("03-FEB-00"), "SH_CLERK",
                                            new Long(2800), new Long(120),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(184), "Nandita",
                                            "Sarchand", "NSARCHAN",
                                            "650.509.1876",
                                            sdf.parse("27-JAN-96"), "SH_CLERK",
                                            new Long(4200), new Long(121),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(185), "Alexis", "Bull",
                                            "ABULL", "650.509.2876",
                                            sdf.parse("20-FEB-97"), "SH_CLERK",
                                            new Long(4100), new Long(121),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(186), "Julia",
                                            "Dellinger", "JDELLING",
                                            "650.509.3876",
                                            sdf.parse("24-JUN-98"), "SH_CLERK",
                                            new Long(3400), new Long(121),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(187), "Anthony", "Cabrio",
                                            "ACABRIO", "650.509.4876",
                                            sdf.parse("07-FEB-99"), "SH_CLERK",
                                            new Long(3000), new Long(121),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(188), "Kelly", "Chung",
                                            "KCHUNG", "650.505.1876",
                                            sdf.parse("14-JUN-97"), "SH_CLERK",
                                            new Long(3800), new Long(122),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(189), "Jennifer", "Dilly",
                                            "JDILLY", "650.505.2876",
                                            sdf.parse("13-AUG-97"), "SH_CLERK",
                                            new Long(3600), new Long(122),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(190), "Timothy", "Gates",
                                            "TGATES", "650.505.3876",
                                            sdf.parse("11-JUL-98"), "SH_CLERK",
                                            new Long(2900), new Long(122),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(191), "Randall",
                                            "Perkins", "RPERKINS",
                                            "650.505.4876",
                                            sdf.parse("19-DEC-99"), "SH_CLERK",
                                            new Long(2500), new Long(122),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(192), "Sarah", "Bell",
                                            "SBELL", "650.501.1876",
                                            sdf.parse("04-FEB-96"), "SH_CLERK",
                                            new Long(4000), new Long(123),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(193), "Britney",
                                            "Everett", "BEVERETT",
                                            "650.501.2876",
                                            sdf.parse("03-MAR-97"), "SH_CLERK",
                                            new Long(3900), new Long(123),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(194), "Samuel", "McCain",
                                            "SMCCAIN", "650.501.3876",
                                            sdf.parse("01-JUL-98"), "SH_CLERK",
                                            new Long(3200), new Long(123),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(195), "Vance", "Jones",
                                            "VJONES", "650.501.4876",
                                            sdf.parse("17-MAR-99"), "SH_CLERK",
                                            new Long(2800), new Long(123),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(196), "Alana", "Walsh",
                                            "AWALSH", "650.507.9811",
                                            sdf.parse("24-APR-98"), "SH_CLERK",
                                            new Long(3100), new Long(124),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(197), "Kevin", "Feeney",
                                            "KFEENEY", "650.507.9822",
                                            sdf.parse("23-MAY-98"), "SH_CLERK",
                                            new Long(3000), new Long(124),
                                            new Long(50)));
            employeesList.add(new Employees(new Long(103), "Alexander",
                                            "Hunold", "AHUNOLD",
                                            "590.423.4567",
                                            sdf.parse("03-JAN-90"), "IT_PROG",
                                            new Long(9000), new Long(102),
                                            new Long(60)));
            employeesList.add(new Employees(new Long(104), "Bruce", "Ernst",
                                            "BERNST", "590.423.4568",
                                            sdf.parse("21-MAY-91"), "IT_PROG",
                                            new Long(6000), new Long(103),
                                            new Long(60)));
            employeesList.add(new Employees(new Long(105), "David", "Austin",
                                            "DAUSTIN", "590.423.4569",
                                            sdf.parse("25-JUN-97"), "IT_PROG",
                                            new Long(4800), new Long(103),
                                            new Long(60)));
            employeesList.add(new Employees(new Long(106), "Valli",
                                            "Pataballa", "VPATABAL",
                                            "590.423.4560",
                                            sdf.parse("05-FEB-98"), "IT_PROG",
                                            new Long(4800), new Long(103),
                                            new Long(60)));
            employeesList.add(new Employees(new Long(107), "Diana", "Lorentz",
                                            "DLORENTZ", "590.423.5567",
                                            sdf.parse("07-FEB-99"), "IT_PROG",
                                            new Long(4200), new Long(103),
                                            new Long(60)));
            employeesList.add(new Employees(new Long(204), "Hermann", "Baer",
                                            "HBAER", "515.123.8888",
                                            sdf.parse("07-JUN-94"), "PR_REP",
                                            new Long(10000), new Long(101),
                                            new Long(70)));
            employeesList.add(new Employees(new Long(145), "John", "Russell",
                                            "JRUSSEL", "011.44.1344.429268",
                                            sdf.parse("01-OCT-96"), "SA_MAN",
                                            new Long(14000), new Long(100),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(146), "Karen", "Partners",
                                            "KPARTNER", "011.44.1344.467268",
                                            sdf.parse("05-JAN-97"), "SA_MAN",
                                            new Long(13500), new Long(100),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(147), "Alberto",
                                            "Errazuriz", "AERRAZUR",
                                            "011.44.1344.429278",
                                            sdf.parse("10-MAR-97"), "SA_MAN",
                                            new Long(12000), new Long(100),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(148), "Gerald",
                                            "Cambrault", "GCAMBRAU",
                                            "011.44.1344.619268",
                                            sdf.parse("15-OCT-99"), "SA_MAN",
                                            new Long(11000), new Long(100),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(149), "Eleni", "Zlotkey",
                                            "EZLOTKEY", "011.44.1344.429018",
                                            sdf.parse("29-JAN-00"), "SA_MAN",
                                            new Long(10500), new Long(100),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(150), "Peter", "Tucker",
                                            "PTUCKER", "011.44.1344.129268",
                                            sdf.parse("30-JAN-97"), "SA_REP",
                                            new Long(10000), new Long(145),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(151), "David",
                                            "Bernstein", "DBERNSTE",
                                            "011.44.1344.345268",
                                            sdf.parse("24-MAR-97"), "SA_REP",
                                            new Long(9500), new Long(145),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(152), "Peter", "Hall",
                                            "PHALL", "011.44.1344.478968",
                                            sdf.parse("20-AUG-97"), "SA_REP",
                                            new Long(9000), new Long(145),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(153), "Christopher",
                                            "Olsen", "COLSEN",
                                            "011.44.1344.498718",
                                            sdf.parse("30-MAR-98"), "SA_REP",
                                            new Long(8000), new Long(145),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(154), "Nanette",
                                            "Cambrault", "NCAMBRAU",
                                            "011.44.1344.987668",
                                            sdf.parse("09-DEC-98"), "SA_REP",
                                            new Long(7500), new Long(145),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(155), "Oliver", "Tuvault",
                                            "OTUVAULT", "011.44.1344.486508",
                                            sdf.parse("23-NOV-99"), "SA_REP",
                                            new Long(7000), new Long(145),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(156), "Janette", "King",
                                            "JKING", "011.44.1345.429268",
                                            sdf.parse("30-JAN-96"), "SA_REP",
                                            new Long(10000), new Long(146),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(157), "Patrick", "Sully",
                                            "PSULLY", "011.44.1345.929268",
                                            sdf.parse("04-MAR-96"), "SA_REP",
                                            new Long(9500), new Long(146),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(158), "Allan", "McEwen",
                                            "AMCEWEN", "011.44.1345.829268",
                                            sdf.parse("01-AUG-96"), "SA_REP",
                                            new Long(9000), new Long(146),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(159), "Lindsey", "Smith",
                                            "LSMITH", "011.44.1345.729268",
                                            sdf.parse("10-MAR-97"), "SA_REP",
                                            new Long(8000), new Long(146),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(160), "Louise", "Doran",
                                            "LDORAN", "011.44.1345.629268",
                                            sdf.parse("15-DEC-97"), "SA_REP",
                                            new Long(7500), new Long(146),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(161), "Sarath", "Sewall",
                                            "SSEWALL", "011.44.1345.529268",
                                            sdf.parse("03-NOV-98"), "SA_REP",
                                            new Long(7000), new Long(146),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(162), "Clara", "Vishney",
                                            "CVISHNEY", "011.44.1346.129268",
                                            sdf.parse("11-NOV-97"), "SA_REP",
                                            new Long(10500), new Long(147),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(163), "Danielle",
                                            "Greene", "DGREENE",
                                            "011.44.1346.229268",
                                            sdf.parse("19-MAR-99"), "SA_REP",
                                            new Long(9500), new Long(147),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(164), "Mattea", "Marvins",
                                            "MMARVINS", "011.44.1346.329268",
                                            sdf.parse("24-JAN-00"), "SA_REP",
                                            new Long(7200), new Long(147),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(165), "David", "Lee",
                                            "DLEE", "011.44.1346.529268",
                                            sdf.parse("23-FEB-00"), "SA_REP",
                                            new Long(6800), new Long(147),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(166), "Sundar", "Ande",
                                            "SANDE", "011.44.1346.629268",
                                            sdf.parse("24-MAR-00"), "SA_REP",
                                            new Long(6400), new Long(147),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(167), "Amit", "Banda",
                                            "ABANDA", "011.44.1346.729268",
                                            sdf.parse("21-APR-00"), "SA_REP",
                                            new Long(6200), new Long(147),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(168), "Lisa", "Ozer",
                                            "LOZER", "011.44.1343.929268",
                                            sdf.parse("11-MAR-97"), "SA_REP",
                                            new Long(11500), new Long(148),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(169), "Harrison", "Bloom",
                                            "HBLOOM", "011.44.1343.829268",
                                            sdf.parse("23-MAR-98"), "SA_REP",
                                            new Long(10000), new Long(148),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(170), "Tayler", "Fox",
                                            "TFOX", "011.44.1343.729268",
                                            sdf.parse("24-JAN-98"), "SA_REP",
                                            new Long(9600), new Long(148),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(171), "William", "Smith",
                                            "WSMITH", "011.44.1343.629268",
                                            sdf.parse("23-FEB-99"), "SA_REP",
                                            new Long(7400), new Long(148),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(172), "Elizabeth",
                                            "Bates", "EBATES",
                                            "011.44.1343.529268",
                                            sdf.parse("24-MAR-99"), "SA_REP",
                                            new Long(7300), new Long(148),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(173), "Sundita", "Kumar",
                                            "SKUMAR", "011.44.1343.329268",
                                            sdf.parse("21-APR-00"), "SA_REP",
                                            new Long(6100), new Long(148),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(174), "Ellen", "Abel",
                                            "EABEL", "011.44.1644.429267",
                                            sdf.parse("11-MAY-96"), "SA_REP",
                                            new Long(11000), new Long(149),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(175), "Alyssa", "Hutton",
                                            "AHUTTON", "011.44.1644.429266",
                                            sdf.parse("19-MAR-97"), "SA_REP",
                                            new Long(8800), new Long(149),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(176), "Jonathon",
                                            "Taylor", "JTAYLOR",
                                            "011.44.1644.429265",
                                            sdf.parse("24-MAR-98"), "SA_REP",
                                            new Long(8600), new Long(149),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(177), "Jack",
                                            "Livingston", "JLIVINGS",
                                            "011.44.1644.429264",
                                            sdf.parse("23-APR-98"), "SA_REP",
                                            new Long(8400), new Long(149),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(179), "Charles",
                                            "Johnson", "CJOHNSON",
                                            "011.44.1644.429262",
                                            sdf.parse("04-JAN-00"), "SA_REP",
                                            new Long(6200), new Long(149),
                                            new Long(80)));
            employeesList.add(new Employees(new Long(100), "Steven", "King",
                                            "SKING", "515.123.4567",
                                            sdf.parse("17-JUN-87"), "AD_PRES",
                                            new Long(24000), null,
                                            new Long(90)));
            employeesList.add(new Employees(new Long(101), "Neena", "Kochhar",
                                            "NKOCHHAR", "515.123.4568",
                                            sdf.parse("21-SEP-89"), "AD_VP",
                                            new Long(17000), new Long(100),
                                            new Long(90)));
            employeesList.add(new Employees(new Long(102), "Lex", "De Haan",
                                            "LDEHAAN", "515.123.4569",
                                            sdf.parse("13-JAN-93"), "AD_VP",
                                            new Long(17000), new Long(100),
                                            new Long(90)));
            employeesList.add(new Employees(new Long(108), "Nancy",
                                            "Greenberg", "NGREENBE",
                                            "515.124.4569",
                                            sdf.parse("17-AUG-94"), "FI_MGR",
                                            new Long(12000), new Long(101),
                                            new Long(100)));
            employeesList.add(new Employees(new Long(109), "Daniel", "Faviet",
                                            "DFAVIET", "515.124.4169",
                                            sdf.parse("16-AUG-94"),
                                            "FI_ACCOUNT", new Long(9000),
                                            new Long(108), new Long(100)));
            employeesList.add(new Employees(new Long(110), "John", "Chen",
                                            "JCHEN", "515.124.4269",
                                            sdf.parse("28-SEP-97"),
                                            "FI_ACCOUNT", new Long(8200),
                                            new Long(108), new Long(100)));
            employeesList.add(new Employees(new Long(111), "Ismael", "Sciarra",
                                            "ISCIARRA", "515.124.4369",
                                            sdf.parse("30-SEP-97"),
                                            "FI_ACCOUNT", new Long(7700),
                                            new Long(108), new Long(100)));
            employeesList.add(new Employees(new Long(112), "Jose Manuel",
                                            "Urman", "JMURMAN", "515.124.4469",
                                            sdf.parse("07-MAR-98"),
                                            "FI_ACCOUNT", new Long(7800),
                                            new Long(108), new Long(100)));
            employeesList.add(new Employees(new Long(113), "Luis", "Popp",
                                            "LPOPP", "515.124.4567",
                                            sdf.parse("07-DEC-99"),
                                            "FI_ACCOUNT", new Long(6900),
                                            new Long(108), new Long(100)));
        } catch (ParseException e) {
            // arrrrrgs
            e.printStackTrace();
        }
        return employeesList;
    }
    
    public void addEmployee(Employees e){
        if(employeesList==null){
            //populate departments
            this.getEmployeesList();
        }
        employeesList.add(e);
    }
    
    public void removeEmployee (Employees e){
        if(employeesList==null){
            //populate departments
            this.getEmployeesList();
        }
        
        for(Employees emp : employeesList){
            if (emp.getEmployeeId().equals(e.getEmployeeId())){
                employeesList.remove(emp);
                return;
            }
        }
    }
    
    public void updateEmployee (Employees e){
        if(employeesList==null){
            //populate departments
            this.getEmployeesList();
        }
        
        for(Employees emp : employeesList){
            if (emp.getEmployeeId().equals(e.getEmployeeId())){
                employeesList.remove(emp);
                employeesList.add(emp);
                return;
            }
        }
    }
    
    public ArrayList<Employees> getEmployeesByDepartmentId(Long departmentId){
      ArrayList<Employees> employeesByDepartment = new ArrayList<Employees>();
      if(employeesList==null){
          //populate departments
          this.getEmployeesList();
      }
        
      for(Employees emp : employeesList){
          if (emp.getDepartmentId().equals(departmentId)){                
              employeesByDepartment.add(emp);
          }
      }
      return employeesByDepartment;
    }
    
    public ArrayList<Employees> getEmployeeById(Long employeeId){
      ArrayList<Employees> _list = new ArrayList<Employees>();
      
      if(employeesList==null){
          //populate departments
          this.getEmployeesList();
      }
        
      for(Employees emp : employeesList){
          if (emp.getEmployeeId().equals(employeeId)){                
              _list.add(emp);
              return _list;
          }
      }
      return _list;
    }    
}
