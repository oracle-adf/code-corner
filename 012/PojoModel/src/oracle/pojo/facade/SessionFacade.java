package oracle.pojo.facade;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import oracle.pojo.entities.Departments;
import oracle.pojo.entities.Employees;
import oracle.pojo.entities.Locations;
import oracle.pojo.factory.DepartmentsList;
import oracle.pojo.factory.EmployeesList;
import oracle.pojo.factory.LocationsList;

/**
 * Pagination example for POJO. Departments and Employees data is provided
 * in range defined by the RangeSize property on the iterator in the PageDef
 * file. The DataControls.dcx file must be configured as shown below
 * 
 *  <bean-definition BeanClass="oracle.pojo.facade.SessionFacade"
 *     DataControlHandler="oracle.adf.model.adapter.bean.DataFilterHandler"
 *     xmlns="http://xmlns.oracle.com/adfm/adapter/bean"/>
 * 
 * @Frank Nimphius
 */
public class SessionFacade implements Serializable{
    
    //order is important
    private EmployeesList       employees   = new EmployeesList();
    private DepartmentsList     departments = new DepartmentsList(employees);
    private LocationsList       locations   = new LocationsList(departments);
    
    public SessionFacade() {
    }
    
    public List<Locations> getAllLocations(){
        return locations.getLocationsList();
    }

/*
 * QUERY Departments and handle pagination
 */
    public List<Departments> getAllDepartments(){
        System.out.println("Sorry - no paging !");
       return departments.getDepartmentsList();
    }

    //method called by the ADF binding layer to perform paging
    public List<Departments> getAllDepartments(int index, int range){
        System.out.println("Pagination Departments ! from "+index+" for length "+range);
        
        ArrayList<Departments> rangeList = new ArrayList<Departments>();
        
        for (int i = index; i < departments.getDepartmentsList().size() && i < index + range;  i++) {   
            rangeList.add(departments.getDepartmentsList().get(i));
        }

        return rangeList;
    }
    
    //method required by the ADF binding layer to perform paging
    public long getAllDepartmentsSize(){
        return departments.getDepartmentsList().size();
    }


    /*
     * QUERY Employees and handle pagination
     */
        public List<Employees> getAllEmployees(){
            System.out.println("Sorry - no paging !");
           return employees.getEmployeesList();
        }

        //method called by the ADF binding layer to perform paging
        public List<Employees> getAllEmployees(int index, int range){
            
            System.out.println("Pagination Employees ! from "+index+" for length "+range);            
            
            ArrayList<Employees> rangeList = new ArrayList<Employees>();            
            for (int i = index;  i < employees.getEmployeesList().size() && i < index + range;  i++) {   
                rangeList.add(employees.getEmployeesList().get(i));
            }
            return rangeList;
        }
        
        //method required by the ADF binding layer to perform paging
        public long getAllEmployeesSize(){
            return employees.getEmployeesList().size();
        }




    public List<Departments> getDepartmentsLookUp(){
        return departments.getDepartmentsList();
    }

    public ArrayList<Employees> findEmployeeById(Long employeeId){
            return employees.getEmployeeById(employeeId);
    }

    public List<Employees> findEmployeesByLastName(String _lastname){
        
        ArrayList<Employees> _list = (ArrayList<Employees>) employees.getEmployeesList();
                       
        boolean useWildCard = false;
        String srchString = null;

        useWildCard = _lastname.endsWith("%")?true:false;
        if (useWildCard){
            srchString = _lastname.substring(0,_lastname.length()-1);
        }
        else{
            srchString = _lastname;
        }

        ArrayList<Employees> returnList = new ArrayList<Employees>();

            for (Employees employee : _list) {
                        if(useWildCard){
                            if (employee.getLastname().toLowerCase().indexOf(srchString.toLowerCase(),0)>-1)  {
                               returnList.add(employee);
                            }
                        }
                        else{
                            if (employee.getLastname().equalsIgnoreCase(srchString))  {
                                returnList.add(employee);
                       }
                }
        }
        return returnList;
    }

    public List<Employees> findAllEmployeesByDepartmentId(Long departmentId){
            return employees.getEmployeesByDepartmentId(departmentId);
    }
    
    
    public void mergeEmployee(Employees emp){
        this.doCommit(emp);
    }
    
    public void mergeDepartment(Departments dept){
        this.doCommit(dept);
    }
    
    public void mergeLocations(Locations loc){
        this.doCommit(loc);
    }
    
    public void doCommit (Object o){
        
        if (o instanceof Departments){
            departments.updateDepartment((Departments) o);
            return;
        }
        else if (o instanceof Employees){            
            employees.updateEmployee((Employees) o);
            return;
        }
        else if (o instanceof Locations){            
            locations.updateLocations((Locations)o);
            return;
        }
    }
}
