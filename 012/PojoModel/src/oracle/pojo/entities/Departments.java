package oracle.pojo.entities;

import java.io.Serializable;

import java.util.List;

import oracle.pojo.factory.EmployeesList;

/**
 * Departments Entity
 * @author Frank Nimphius
 * @version 11.0
 */
public class Departments implements Serializable {
    
    private Long departmentId;
    private String departmentName;
    private Long managerId;
    private Long locationId;
    private List<Employees> employees = null;
    EmployeesList employeesList = null;
    
    public Departments() {
            
    }
    
    public Departments(Long _departmentId,
                       String _departmentName,
                       Long _managerId,
                       Long _locationId,
                       EmployeesList list
                       ){
        departmentId = _departmentId;
        departmentName = _departmentName;
        managerId = _managerId;
        locationId = _locationId;
        employeesList = list;
    };

    public void setDepartmentId(Long _departmentId) {
        this.departmentId = _departmentId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentName(String _departmentName) {
        this.departmentName = _departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setManagerId(Long _managerId) {
        this.managerId = _managerId;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setLocationId(Long _locationId) {
        this.locationId = _locationId;
    }

    public Long getLocationId() {
        return locationId;
    }
/*
    public void setEmployees(List _employeesList) {
        this.employees = _employeesList;
    }
*/
    public List<Employees> getEmployees() {
        return employeesList.getEmployeesByDepartmentId(this.getDepartmentId());
    }
}
