package oracle.pojo.entities;

import java.io.Serializable;

import java.util.List;

import oracle.pojo.factory.DepartmentsList;

/**
 * Locations Entity
 * @author Frank Nimphius
 * @version 11.0
 */
public class Locations implements Serializable{
    
    private Long locationId;
    private String streetAddress;
    private String postalCode;
    private String city;
    private String stateProvince;
    private List departments = null;
    private DepartmentsList departmentsList = null;
    
    public Locations() {        
    }
    
    public Locations (Long _locationId,
                      String _streetAddress,
                      String _postalCode,
                      String _city,
                      String _stateProvince,
                      DepartmentsList list){
   
    locationId = _locationId;
    streetAddress = _streetAddress;
    postalCode = _postalCode;
    city=_city;
    stateProvince = _stateProvince;
    departments = null;
    departmentsList = list;                      
   }

    public void setLocationId(Long _locationId) {
        this.locationId = _locationId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setStreetAddress(String _streetAddress) {
        this.streetAddress = _streetAddress;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setPostalCode(String _postalCode) {
        this.postalCode = _postalCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setCity(String _city) {
        this.city = _city;
    }

    public String getCity() {
        return city;
    }

    public void setStateProvince(String _stateProvince) {
        this.stateProvince = _stateProvince;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setDepartments(List _departmentsList) {
    }

    public List<Departments> getDepartments() {
        return departmentsList.getDepartmentsByLocationId(this.getLocationId());
    }
/*
    public void setDepartmentsList(DepartmentsList departmentsList) {
        this.departmentsList = departmentsList;
    }
*/
    public DepartmentsList getDepartmentsList() {
        return departmentsList;
    }
}
