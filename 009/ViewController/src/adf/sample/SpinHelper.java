package adf.sample;

import java.util.List;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichCarousel;
import oracle.adf.view.rich.event.CarouselSpinEvent;

import oracle.jbo.Key;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.model.CollectionModel;


public class SpinHelper {
    public SpinHelper() {
    }


    /*
     * The action handler below is generic and can be used with any
     * ADF bound implementation of the af:carousel component. The method
     * is invoked from the CarouselSpinListener property using EL and
     * ensures that the selected item is set as the current row of the 
     * underlying ADF tree binding. This allows developers to build
     * master/detail forms with the carousel as the master data set. 
     * 
     * This code is generic and does not store any state, it can be stored in 
     * a managed bean with a scope of NONE. 
     * 
     * Make sure that you setup a PartialTriggers property reference between the 
     * child component (e.g. a form or table, and the carousel so that the detail
     * is refreshed when the carousel spins
     * 
     * Note that this code accesses the ADF binding layer without any knowledge 
     * about namings in the PageDef file. It also works with the JU* binding classes, 
     * avoiding any use of the FacesCtrl* private classes. 
     */
    public void onSpin(CarouselSpinEvent carouselSpinEvent) {
        //get the selected item key - an instance of java.util.List
        List currentSelectedKey = (List) carouselSpinEvent.getNewItemKey();
        //get a handle to the carousel component instance. We need this to 
        //generically access the binding layer
        RichCarousel carousel = (RichCarousel) carouselSpinEvent.getSource();
        //get the Trinidad CollectionModel for this component
        CollectionModel         componentModel = (CollectionModel) carousel.getValue();
        //get the ADF tree binding from the CollectionModel. This code also works with
        //tables and trees and thus is worth to remember
        JUCtrlHierBinding       carouselTreeBinding = (JUCtrlHierBinding) componentModel.getWrappedData();
        //get the selected node
        JUCtrlHierNodeBinding   selectedCarouselItemNode = carouselTreeBinding.findNodeByKeyPath(currentSelectedKey);
        //get the row key to make it the current key in the ADF iterator
        Key currentCarouselItemKey = selectedCarouselItemNode.getRowKey();
        //You can access the iterator binding from the tree binding so that you don't need
        //any knowledge about the namings in the PageDef file - Cool, he ?
        DCIteratorBinding  dcIterBinding = carouselTreeBinding.getIteratorBinding();
        //make the row the current row
        dcIterBinding.setCurrentRowWithKey(currentCarouselItemKey.toStringFormat(true));
    }
}
